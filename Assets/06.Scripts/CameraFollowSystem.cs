using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowSystem : MonoBehaviour
{
    public PlayerCmpt playerCmpt;
    public Camera camera;

    //public Vector3 offset;
    //public Vector3 lookPoint;

    public void Start()
    {
        playerCmpt = World.Instance.human;
        camera = Camera.main;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        camera.transform.position =
            playerCmpt.transform.position + playerCmpt.transform.rotation * playerCmpt.eyesPosition;
        camera.transform.rotation = playerCmpt.transform.rotation;
    }
}