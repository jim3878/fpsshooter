using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;

public class BulletSystem : MonoBehaviour
{
    public List<GameObject> bulletPrefabs;
    public List<GameObject> explosionPrefabs;

    public BulletTransferCmpt bulletTransfer;
    public Dictionary<string, GameObject> bulletDictionary;
    public Dictionary<string, GameObject> explosionDictionary;


    [FormerlySerializedAs("bulletControllers")]
    public List<BulletToken> bulletTokens = new List<BulletToken>();


    private void Start()
    {
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("Bullet"), LayerMask.NameToLayer("Bullet"));

        bulletDictionary = new Dictionary<string, GameObject>();
        explosionDictionary = new Dictionary<string, GameObject>();

        bulletTransfer = World.Instance.bulletTransfer;

        foreach (var prefab in bulletPrefabs)
        {
            bulletDictionary.Add(prefab.name, prefab);
        }

        foreach (var prefab in explosionPrefabs)
        {
            explosionDictionary.Add(prefab.name, prefab);
        }
    }

    // Update is called once per frame
    void Update()
    {
        InstantiateBullet();
        UpdateBullet();
    }

    private void InstantiateBullet()
    {
        foreach (var command in bulletTransfer.bulletCommands)
        {
            var bulletPrefab = bulletDictionary[command.bulletName];
            var bullet = Instantiate(bulletPrefab);
            bullet.transform.position = command.hitPosition;
            var token = bullet.AddComponent<BulletToken>();
            token.bulletCommand = command;
            bulletTokens.Add(token);
        }

        bulletTransfer.bulletCommands.Clear();
    }

    private void UpdateBullet()
    {
        foreach (var controller in bulletTokens)
        {
            var command = controller.bulletCommand;

            if (IsOutOfTime(command))
            {
                controller.isClear = true;
                Destroy(controller.gameObject);
                continue;
            }

            if (controller.isHit)
            {
                Damage(controller);
                var prefab = explosionDictionary[command.explosionName];
                var explosion = GameObject.Instantiate(prefab);
                explosion.transform.position = controller.transform.position;
                explosion.transform.rotation = controller.transform.rotation;
                controller.isClear = true;
                Destroy(controller.gameObject);
                continue;
            }

            controller.transform.position += command.direction.normalized * command.speed * Time.deltaTime;
        }

        bulletTokens.RemoveAll(c => c.isClear);
    }

    private void Damage(BulletToken token)
    {
        var lifeToken = token.target.GetComponent<LifeCmpt>();
        if (lifeToken == null) return;

        Debug.Log("Damage");
        lifeToken.damageCommands.Add(new DamageCommand
        {
            damage = token.bulletCommand.damage
        });
    }

    private bool IsOutOfTime(BulletCommand command)
    {
        return (Time.time - command.startTime) > command.duration;
    }
}

[System.Serializable]
public class BulletCommand
{
    public string bulletName;
    public string explosionName;
    public Vector3 hitPosition;
    public float speed;
    public float startTime;
    public float duration;
    public float damage;
    public Vector3 direction;
}