using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputCmpt : MonoBehaviour
{
    public Vector3 movement;
    public Vector3 lastMousePosition;
    public Vector3 mouseDelta;
    public Vector2 scrollDelta;
    public bool switchToWeapon1;
    public bool switchToWeapon2;
    public bool jump;
    public bool triggerDown;
    public bool triggerRelease;
    public bool reload;
    public bool triggerPress;
    public bool aim;
}