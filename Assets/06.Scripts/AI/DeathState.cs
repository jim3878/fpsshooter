using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{

    public class DeathState : EnemyState
    {
        public DeathState(PlayerCmpt player) : base(player) { }

        public override void Execute()
        {
            base.Execute();
            var life = _player.GetComponent<LifeCmpt>();
            if (life.health <= 0)
            {
                var rigidBody = life.GetComponent<Rigidbody>();
                rigidBody.constraints = RigidbodyConstraints.None;
            }
        }
    }
}