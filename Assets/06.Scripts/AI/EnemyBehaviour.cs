using System.Collections;
using System.Collections.Generic;
using AI;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    public PlayerCmpt npc;
    public StateMachine stateMachine;

    void Start()
    {
        stateMachine = new StateMachine();
        stateMachine.TransitionTo(new RandomMoveState(npc));
    }

    // Update is called once per frame
    void Update()
    {
        stateMachine.Update();
    }
}