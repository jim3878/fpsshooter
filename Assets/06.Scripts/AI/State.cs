namespace AI
{
    public class EnemyState : State
    {
        protected PlayerCmpt _player;

        public EnemyState(PlayerCmpt player)
        {
            _player = player;
        }
    }

    public abstract class State
    {
        public StateMachine stateMachine;

        public virtual void Enter()
        {
        }

        public virtual void Execute()
        {
        }

        public virtual void Exit()
        {
        }
    }
}