using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{

    public class TraceHumanState : EnemyState
    {
        private const float Velocity = 3f;


        public TraceHumanState(PlayerCmpt player) : base(player)
        {
        }


        public override void Execute()
        {
            base.Execute();
            var life = _player.GetComponent<LifeCmpt>();
            if (life.health <= 0)
            {
                stateMachine.TransitionTo(new DeathState(_player));
            }
            Debug.Log("trace");

            var human = World.Instance.human;

            var rigidbody = _player.GetComponent<Rigidbody>();

            var forward = human.transform.position - _player.transform.position;

            rigidbody.velocity = forward.normalized * Velocity;

            var rotate = Quaternion.LookRotation(forward);
            _player.transform.rotation = rotate;

            var distance = forward.magnitude;

            if (distance < _player.minDistance)
            {
                stateMachine.TransitionTo(new AttackPlayerState(_player));
            }
            if (distance > _player.maxDistance)
            {
                stateMachine.TransitionTo(new WaitState(_player));
            }

        }


    }

}