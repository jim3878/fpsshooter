using Unity.VisualScripting;
using UnityEngine;

namespace AI
{
    public class WaitState : EnemyState
    {
        private const float Duration = 2f;
        private float _startTime;


        public override void Enter()
        {
            _startTime = Time.time;
        }

        public override void Execute()
        {
            //Debug.Log("WaitState " + (Time.time - _startTime));
            if (Time.time > _startTime + Duration)
            {
                stateMachine.TransitionTo(new RandomMoveState(_player));
            }
            var life = _player.GetComponent<LifeCmpt>();
            if (life.health <= 0)
            {
                stateMachine.TransitionTo(new DeathState(_player));
            }

            var human = World.Instance.human;
            if ((human.transform.position - _player.transform.position).magnitude < 4f)
            {
                stateMachine.TransitionTo(new AttackPlayerState(_player));
            }
        }

        public WaitState(PlayerCmpt player) : base(player)
        {
        }
    }
}