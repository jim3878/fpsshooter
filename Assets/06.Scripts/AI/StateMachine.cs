namespace AI
{
    public class StateMachine
    {
        private State _currentState;

        public void Update()
        {
            if (_currentState != null)
                _currentState.Execute();
        }

        public void TransitionTo(State state)
        {
            if (_currentState != null)
                _currentState.Exit();
        
            state.stateMachine = this;
            state.Enter();
            _currentState = state;
        }
    }
}