using System.Numerics;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

namespace AI
{
    public class RandomMoveState : EnemyState
    {
        private const float Duration = 2f;
        private const float Velocity = 3f;
        private const float RotateVelocity = 360f;

        private float _lastStartTime;
        private Vector3 _lastForward;
        private Quaternion _lastRotate;



        public override void Enter()
        {
            _lastStartTime = Time.time;
            var angle = Random.Range(0, 360);

            _lastRotate = Quaternion.Euler(0, angle, 0);
            _lastForward = _lastRotate * Vector3.forward;
            //_player.transform.rotation = Quaternion.Euler(0, angle, 0);
        }

        public override void Execute()
        {
            //Debug.Log("move");
            if (Time.time > _lastStartTime + Duration)
            {
                stateMachine.TransitionTo(new WaitState(_player));
                return;
            }

            var life = _player.GetComponent<LifeCmpt>();
            if (life.health <= 0)
            {
                stateMachine.TransitionTo(new DeathState(_player));
            }

            var human = World.Instance.human;
            var distance = (human.transform.position - _player.transform.position).magnitude;
            //Debug.Log("distance=" + distance);
            if (distance < 4f)
            {
                stateMachine.TransitionTo(new AttackPlayerState(_player));
                return;
            }

            //Vector3.MoveTowards()
            _player.transform.rotation = Quaternion.RotateTowards(_player.transform.rotation, _lastRotate, RotateVelocity * Time.deltaTime);
            _player.GetComponent<Rigidbody>().velocity = _lastForward * Velocity + 
                new Vector3(0, _player.GetComponent<Rigidbody>().velocity.y, 0);
        }

        public RandomMoveState(PlayerCmpt player) : base(player)
        {
        }
    }
}