using UnityEngine;

namespace AI
{

    public class AttackPlayerState : EnemyState
    {
        private float _lastFireTime;
        private float _fireDeltaTime = 0.2f;
        private int _bulletCount = 30;


        public AttackPlayerState(PlayerCmpt player) : base(player)
        {
        }

        public override void Enter()
        {
            base.Enter();
            _lastFireTime = Time.time;
            Fire();
        }

        public override void Execute()
        {
            base.Execute();

            var life = _player.GetComponent<LifeCmpt>();
            if (life.health <= 0)
            {
                stateMachine.TransitionTo(new DeathState(_player));
            }
            Debug.Log("attack");
            if (_bulletCount <= 0)
            {
                stateMachine.TransitionTo(new WaitState(_player));
                return;
            }

            var human = World.Instance.human;
            var distance = (human.transform.position - _player.transform.position).magnitude;
            if (distance >= _player.minDistance)
            {
                stateMachine.TransitionTo(new TraceHumanState(_player));
            }

            LookAt();
            Fire();
        }

        private void LookAt()
        {
            var human = World.Instance.human;

            _player.transform.LookAt(human.transform.position);
        }

        private void Fire()
        {
            if (Time.time - _lastFireTime < _fireDeltaTime) return;

            Debug.Log("fire");
            _lastFireTime = Time.time;
            _bulletCount--;
            var bulletTransfer = World.Instance.bulletTransfer;
            bulletTransfer.bulletCommands.Add(new BulletCommand
            {
                bulletName = "Bullet",
                explosionName = "Explosion",
                hitPosition = _player.WeaponCmpts[0].firePoint.position,
                speed = 100,
                startTime = Time.time,
                duration = 1f,
                damage = 5,
                direction = _player.transform.forward
            });
        }
    }
}