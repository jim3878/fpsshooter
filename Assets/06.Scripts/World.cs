using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class World : MonoBehaviour
{
    public static World Instance;

    public List<WeaponCmpt> weaponTokens = new List<WeaponCmpt>();
    public InputCmpt inputCmpt;
    public BulletTransferCmpt bulletTransfer;
    public PlayerCmpt human;
    public WeaponTextCmpt weaponText;
 
    public void Awake()
    {
        Instance = this;
    }
}