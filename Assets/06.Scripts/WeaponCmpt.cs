using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponCmpt : MonoBehaviour
{
    public bool isEquip;
    public bool isReloading;
    public int maxBullet;
    public int currentBullet;
    public Transform firePoint;
}