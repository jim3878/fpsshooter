using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletToken : MonoBehaviour
{
    public BulletCommand bulletCommand;
    public bool isHit;
    public HittableCmpt target;
    public bool isClear;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.GetComponent<HittableCmpt>() != null)
        {
            target = collision.collider.GetComponent<HittableCmpt>();
            isHit = true;
        }
    }
}