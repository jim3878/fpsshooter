using System;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

public class PlayerControlSystem : MonoBehaviour
{
    public Rigidbody rigidbody;
    public InputCmpt inputCmpt;
    public PlayerCmpt playerCmpt;

    public Vector2 rotateClampY;
    
    public float rotateSpeed, speed;
    public float jumpSpeed;

    private void Start()
    {
        playerCmpt = World.Instance.human;
        rigidbody = playerCmpt.GetComponent<Rigidbody>();
        inputCmpt = World.Instance.inputCmpt;
    }

    private void Update()
    {
        UpdateRotate();
        UpdateMove();
        ChangeWeapon();
        Jump();
    }

    public void ChangeWeapon()
    {
        var input = World.Instance.inputCmpt;

        if (input.switchToWeapon1)
            ChangeWeapon(0);
        if (input.switchToWeapon2)
            ChangeWeapon(1);
    }

    public void ChangeWeapon(int idx)
    {
        var player = World.Instance.human;
        foreach (var playerWeapon in player.WeaponCmpts)
        {
            playerWeapon.isEquip = false;
        }

        player.WeaponCmpts[idx].isEquip = true;
    }

    private void Jump()
    {
        if (World.Instance.inputCmpt.jump)
        {
            rigidbody.velocity = Vector3.up * jumpSpeed;
        }
    }

    private void UpdateMove()
    {
        rigidbody.velocity = new Vector3(0, rigidbody.velocity.y, 0);
        var angle = playerCmpt.transform.rotation.eulerAngles;
        var moveRotate = Quaternion.Euler(0, angle.y, 0);
        playerCmpt.transform.position += moveRotate * World.Instance.inputCmpt.movement * speed * Time.deltaTime;
    }

    private void UpdateRotate()
    {
        
        playerCmpt.rotate += new Vector3(-World.Instance.inputCmpt.mouseDelta.y,
            World.Instance.inputCmpt.mouseDelta.x, 0) * rotateSpeed;
        playerCmpt.rotate.x = Mathf.Clamp(playerCmpt.rotate.x, rotateClampY.x, rotateClampY.y);

        playerCmpt.transform.rotation = Quaternion.Euler(playerCmpt.rotate);
    }
}