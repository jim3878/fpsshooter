using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DamageSystem : MonoBehaviour
{
    public List<LifeCmpt> lifeTokens;

    private void Start()
    {
        lifeTokens = FindObjectsOfType<LifeCmpt>().ToList();
    }

    private void Update()
    {
        foreach (var lifeToken in lifeTokens)
        {
            foreach (var command in lifeToken.damageCommands)
            {
                lifeToken.health = Mathf.Clamp(lifeToken.health - command.damage, 0, lifeToken.maxHealth);
            }

            lifeToken.damageCommands.Clear();
        }
    }
}