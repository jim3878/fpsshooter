using System.Collections;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class RifleWeapon : MonoBehaviour
{
    public WeaponCmpt weapon;
    public GameObject appearance;
    public Transform firePoint;
    private float _moveSpeed = 5;
    private float _rotateSpeed = 360f;
    private float _lastFireTime;
    private float _fireSpeed = 10;
    private Vector3 _shakeRotateMax = new Vector3(1f, 1f, 0f);
    private Vector3 _shakeRotateMin = new Vector3(-1.5f, -1f, 0f);


    // Update is called once per frame
    private float FireDelta => 1f / _fireSpeed;

    void Update()
    {
        UpdateTransform();
        Reload();
        Fire();
    }

    private void Reload()
    {
        if (weapon.isEquip && World.Instance.inputCmpt.reload)
        {
            StartCoroutine(Reloading());
        }
    }

    private IEnumerator Reloading()
    {
        weapon.isReloading = true;
        yield return new WaitForSeconds(1f);
        weapon.isReloading = false;
        weapon.currentBullet = weapon.maxBullet;
    }

    private void UpdateTransform()
    {
        var input = World.Instance.inputCmpt;
        var player = World.Instance.human;


        transform.SetParent(player.transform);
        appearance.SetActive(weapon.isEquip);
        var weaponTransform = weapon.transform;
        // weaponTransform.SetParent();
        var point = input.aim && weapon.isEquip ? player.aimWeaponPoint : player.keepWeaponPoint;
        weaponTransform.localPosition =
            Vector3.MoveTowards(transform.localPosition, point.localPosition, _moveSpeed * Time.deltaTime);
        weaponTransform.localRotation =
            Quaternion.RotateTowards(weaponTransform.localRotation, point.localRotation, _rotateSpeed * Time.deltaTime);
    }

    private void Shake()
    {
        var x = Random.Range(_shakeRotateMax.x, _shakeRotateMin.x);
        var y = Random.Range(_shakeRotateMax.y, _shakeRotateMin.y);
        var z = Random.Range(_shakeRotateMax.z, _shakeRotateMin.z);

        var player = World.Instance.human;
        player.rotate += new Vector3(x, y, z);

        //appearance.transform.DOLocalJump(appearance.transform.localPosition, 1f, 1,FireDelta*0.9f);
        appearance.transform.DOPunchPosition(Vector3.forward * 0.1f, FireDelta * 0.9f);
    }

    private bool HasBullet()
    {
        return weapon.currentBullet > 0;
    }

    private void Fire()
    {
        var inputToken = World.Instance.inputCmpt;
        var bulletTransfer = World.Instance.bulletTransfer;
        if (!weapon.isReloading && weapon.isEquip && HasBullet() && inputToken.triggerPress &&
            Time.time > _lastFireTime + FireDelta)
        {
            Shake();
            weapon.currentBullet -= 1;
            _lastFireTime = Time.time;
            bulletTransfer.bulletCommands.Add(new BulletCommand
            {
                bulletName = "Bullet",
                explosionName = "Explosion",
                hitPosition = firePoint.position,
                speed = 100,
                startTime = Time.time,
                duration = 1f,
                damage = 5,
                direction = transform.forward
            });
        }
    }
}