using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class SampleWeapon : MonoBehaviour
{
    public WeaponCmpt weapon;
    public GameObject appearance;
    public Transform firePoint;
    private float _moveSpeed = 5;
    private float _rotateSpeed = 360f;
    private Vector3 _shakeRotateMax = new Vector3(1f, 1f, 0f);
    private Vector3 _shakeRotateMin = new Vector3(-1.5f, -1f, 0f);

    // Update is called once per frame
    void Update()
    {
        UpdateTransform();
        if (!weapon.isEquip)
        {
            appearance.SetActive(false);
            return;
        }

        Fire();
        Reload();
    }

    private void Reload()
    {
        if (weapon.isEquip && World.Instance.inputCmpt.reload)
        {
            StartCoroutine(Reloading());
        }
    }

    private IEnumerator Reloading()
    {
        weapon.isReloading = true;
        yield return new WaitForSeconds(1f);
        weapon.isReloading = false;
        weapon.currentBullet = weapon.maxBullet;
    }

    private void UpdateTransform()
    {
        var input = World.Instance.inputCmpt;
        var player = World.Instance.human;


        transform.SetParent(player.transform);
        appearance.SetActive(weapon.isEquip);
        var weaponTransform = weapon.transform;
        var point = (input.aim && weapon.isEquip) ? player.aimWeaponPoint : player.keepWeaponPoint;
        weaponTransform.localPosition =
            Vector3.MoveTowards(transform.localPosition, point.localPosition, _moveSpeed * Time.deltaTime);
        weaponTransform.localRotation =
            Quaternion.RotateTowards(weaponTransform.localRotation, point.localRotation, _rotateSpeed * Time.deltaTime);
    }

    private void Shake()
    {
        var x = Random.Range(_shakeRotateMax.x, _shakeRotateMin.x);
        var y = Random.Range(_shakeRotateMax.y, _shakeRotateMin.y);
        var z = Random.Range(_shakeRotateMax.z, _shakeRotateMin.z);

        var player = World.Instance.human;
        player.rotate += new Vector3(x, y, z);
        appearance.transform.DOPunchPosition(Vector3.forward * 0.1f, 0.2f * 0.9f);
    }

    private bool HasBullet()
    {
        return weapon.currentBullet > 0;
    }

    private void Fire()
    {
        var inputToken = World.Instance.inputCmpt;
        var bulletTransfer = World.Instance.bulletTransfer;
        if (!weapon.isReloading && weapon.isEquip && HasBullet() && inputToken.triggerDown)
        {
            Shake();
            weapon.currentBullet -= 1;
            bulletTransfer.bulletCommands.Add(new BulletCommand
            {
                bulletName = "Bullet",
                explosionName = "Explosion",
                hitPosition = firePoint.position,
                speed = 30,
                startTime = Time.time,
                duration = 1f,
                damage = 5,
                direction = transform.forward
            });
        }
    }
}