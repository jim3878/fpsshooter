using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowWeaponUi : MonoBehaviour
{
    public WeaponTextCmpt weaponText;
    public WeaponCmpt weapon;

    void Start()
    {
        weaponText = World.Instance.weaponText;
    }

    // Update is called once per frame
    void Update()
    {
        if (weapon.isEquip)
            weaponText.textGui.text = $"{weapon.currentBullet}/{weapon.maxBullet}";
    }
}