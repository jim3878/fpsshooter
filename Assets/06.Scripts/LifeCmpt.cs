using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeCmpt : MonoBehaviour
{
    public float health;
    public float maxHealth;
    public bool hideStatus;
    public List<DamageCommand> damageCommands = new List<DamageCommand>();
    public List<JumpInfoCommand> jumpInfoCommands = new List<JumpInfoCommand>();
}

[System.Serializable]
public class DamageCommand
{
    public float damage;
}

[System.Serializable]
public class JumpInfoCommand
{
    public string info;
}