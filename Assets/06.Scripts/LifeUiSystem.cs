using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Schema;
using UnityEngine;
using UnityEngine.Serialization;

public class LifeUiSystem : MonoBehaviour
{
    public Canvas lifeCanvas;
    public LifeStatus lifeStatusPrefab;
    public List<LifeCmpt> lifeTokens;
    public List<LifeStatus> lifeStatus = new List<LifeStatus>();

    private void Start()
    {
        lifeTokens = FindObjectsOfType<LifeCmpt>().ToList();

        foreach (var lifeToken in lifeTokens)
        {
            var status = GameObject.Instantiate(lifeStatusPrefab, lifeCanvas.transform);
            lifeStatus.Add(status);
        }
    }

    private void LateUpdate()
    {
        for (var i = 0; i < lifeTokens.Count; i++)
        {
            var token = lifeTokens[i];
            var status = lifeStatus[i];
            var cameraTransform = Camera.main.transform;
            status.gameObject.SetActive(!IsHide(token.transform, cameraTransform) &&
                                        IsInForward(cameraTransform, token.transform) &&
                                        !token.hideStatus);
            Follow(token, status);

            status.hpValue.anchorMax = new Vector2(token.health / token.maxHealth, 1);
        }
    }

    private bool IsInForward(Transform self, Transform target)
    {
        var targetVector = (target.position - self.position).normalized;

        return Vector3.Dot(self.forward, targetVector) > 0;
    }

    private bool IsHide(Transform self, Transform target)
    {
        var ray = new Ray(self.transform.position, target.transform.position - self.transform.position);
        if (Physics.Raycast(ray, out var hitInfo))
        {
            return hitInfo.collider.GetComponent<PlayerCmpt>() == null;
        }

        return false;
    }

    private static void Follow(LifeCmpt cmpt, LifeStatus status)
    {
        var canvasPosition = Camera.main.WorldToScreenPoint(cmpt.transform.position + Vector3.up);
        status.transform.position = canvasPosition;
    }
}