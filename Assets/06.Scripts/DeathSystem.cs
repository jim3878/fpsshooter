using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DeathSystem : MonoBehaviour
{
    public List<LifeCmpt> lifeTokens;

    private void Start()
    {
        lifeTokens = FindObjectsOfType<LifeCmpt>().ToList();
    }

    private void Update()
    {
        foreach (var lifeToken in lifeTokens)
        {
            if (lifeToken.health <= 0)
            {
                var rigidBody = lifeToken.GetComponent<Rigidbody>();
                rigidBody.constraints = RigidbodyConstraints.None;
            }
        }
    }
}