using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorCmpt : MonoBehaviour
{
    public bool isTrigger;
    public Transform doorTransform;


    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerCmpt>() != null)
            isTrigger = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<PlayerCmpt>() != null)
            isTrigger = false;
    }
}
