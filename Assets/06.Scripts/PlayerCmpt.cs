using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class PlayerCmpt : MonoBehaviour
{
    public bool isHuman;
    public Vector3 eyesPosition;
    public Transform aimWeaponPoint;
    public Transform keepWeaponPoint;
    public Rigidbody rigidbody;
    public Vector3 rotate;

    public float minDistance = 5f;
    public float maxDistance = 10f;

    public List<WeaponCmpt> WeaponCmpts;

    public int ammoMax;
    public int ammoLeft;
    public List<BulletCommand> bulletCommands = new List<BulletCommand>();


    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(transform.TransformPoint(eyesPosition), 0.1f);

        
        Gizmos.DrawWireSphere(transform.position, minDistance);
        Gizmos.DrawWireSphere(transform.position, maxDistance);
    }
}