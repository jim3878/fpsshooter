using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HumanHp : MonoBehaviour
{
    public Image hp;

    // Update is called once per frame
    void Update()
    {
        var life = World.Instance.human.GetComponent<LifeCmpt>();

        var progress = life.health / life.maxHealth;
        hp.GetComponent<RectTransform>().anchorMax = new Vector2(progress, 1);
    }
}