using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    public float duration;

    public float size;

    // Start is called before the first frame update
    void Start()
    {
        transform.DOScale(size, duration).SetEase(Ease.OutBack).OnComplete(() => Destroy(gameObject));
    }
}