using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorBehaviour : MonoBehaviour
{
    public DoorState state;
    public DoorCmpt doorCmpt;
    public float speed = 2f;

    public float waitDuration = 1;
    public float startWaitTime;


    private void Start()
    {
        doorCmpt = GetComponent<DoorCmpt>();
    }



    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case DoorState.open:
                if (doorCmpt.isTrigger == false)
                {
                    startWaitTime = Time.time;
                    state = DoorState.close;
                }

                doorCmpt.doorTransform.localPosition =
                    Vector3.MoveTowards(doorCmpt.doorTransform.localPosition, new Vector3(0, -1, 0), speed * Time.deltaTime);
                break;

            case DoorState.close:
                if (doorCmpt.isTrigger == true)
                {
                    state = DoorState.open;
                }
                doorCmpt.doorTransform.localPosition =
                    Vector3.MoveTowards(doorCmpt.doorTransform.localPosition, new Vector3(0, 0, 0), speed * Time.deltaTime);
                break;

            case DoorState.wait:

                if (Time.time > startWaitTime + waitDuration)
                {

                    state = DoorState.close;
                }
                break;
        }
    }


    public enum DoorState
    {
        open,
        wait,
        close
    }
}
