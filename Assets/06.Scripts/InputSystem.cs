using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputSystem : MonoBehaviour
{
    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = true;
    }

    // Update is called once per frame
    void Update()
    {
        var inputToken = World.Instance.inputCmpt;
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.visible ^= true;
        }

        inputToken.jump = Input.GetKeyDown(KeyCode.Space);

        inputToken.triggerDown = Input.GetMouseButtonUp(0);
        inputToken.triggerPress = Input.GetMouseButton(0);
        inputToken.triggerRelease = Input.GetMouseButtonUp(0);
        inputToken.reload = Input.GetKeyDown(KeyCode.R);

        inputToken.aim = Input.GetMouseButton(1);
        inputToken.switchToWeapon1 = Input.GetKeyDown(KeyCode.Alpha1);
        inputToken.switchToWeapon2 = Input.GetKeyDown(KeyCode.Alpha2);


        UpdateMouseDelta(inputToken);
        UpdateMovement(inputToken);
    }

    private void UpdateMouseDelta(InputCmpt inputCmpt)
    {
        inputCmpt.mouseDelta = Vector3.zero;
        inputCmpt.mouseDelta = new Vector3(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"), 0);
    }

    private void UpdateMovement(InputCmpt inputCmpt)
    {
        inputCmpt.movement = Vector3.zero;
        if (Input.GetKey(KeyCode.W))
        {
            inputCmpt.movement += Vector3.forward;
        }

        if (Input.GetKey(KeyCode.A))
        {
            inputCmpt.movement += Vector3.left;
        }

        if (Input.GetKey(KeyCode.S))
        {
            inputCmpt.movement += Vector3.back;
        }

        if (Input.GetKey(KeyCode.D))
        {
            inputCmpt.movement += Vector3.right;
        }

        inputCmpt.movement = inputCmpt.movement.normalized;
    }
}