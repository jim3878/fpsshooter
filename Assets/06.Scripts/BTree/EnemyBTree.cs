using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBTree : MonoBehaviour
{
    private BehaviourNode _root;

    // Start is called before the first frame update
    void Start()
    {
        var player = GetComponent<PlayerCmpt>();
        _root = new SelectNode();

        var liveNode = new SequenceNode();
        liveNode.AddChild(new MoveNode(player));
        liveNode.AddChild(new IdleNode(player));

        var deadNode = new SequenceNode();
        deadNode.AddChild(new DeathConditonNode(player));
        deadNode.AddChild(new DeadNode(player));

        _root.AddChild(deadNode);
        _root.AddChild(liveNode);
    }

    // Update is called once per frame
    void Update()
    {
        _root.Tick();
    }
}
