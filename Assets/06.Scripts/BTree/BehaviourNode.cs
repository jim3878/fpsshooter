using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public abstract class BehaviourNode
{
    public List<BehaviourNode> childNodes = new List<BehaviourNode>();

    public BehaviourNode AddChild(BehaviourNode node)
    {
        childNodes.Add(node);
        return this;
    }

    public abstract NodeState Tick();

    public virtual void Transition() { }
}

public enum NodeState
{
    Success,
    Failure,
    Running,
    Done
}