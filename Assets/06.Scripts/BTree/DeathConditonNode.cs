﻿
using UnityEngine;

public class DeadNode : EnemyNode
{
    public DeadNode(PlayerCmpt player) : base(player)
    {
    }

    public override NodeState Tick()
    {
        var life = _player.GetComponent<LifeCmpt>();
        if (life.health <= 0)
        {
            var rigidBody = life.GetComponent<Rigidbody>();
            rigidBody.constraints = RigidbodyConstraints.None;
        }

        return NodeState.Running;
    }
}
public class DeathConditonNode : EnemyNode
{
    public DeathConditonNode(PlayerCmpt player) : base(player)
    {
    }

    public override NodeState Tick()
    {
        var life = _player.GetComponent<LifeCmpt>();

        if (life.health > 0)
            return NodeState.Failure;
        return NodeState.Done;
    }
}
