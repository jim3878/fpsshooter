using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveNode : EnemyNode
{

    private const float Duration = 2f;
    private const float Velocity = 3f;
    private const float RotateVelocity = 360f;

    private bool _isStart;
    private float _startTime;
    private Quaternion _rotate;
    private Vector3 _moveForward;

    public MoveNode(PlayerCmpt player) : base(player)
    {
    }

    public override NodeState Tick()
    {
        if (!_isStart)
        {
            _startTime = Time.time;
            _isStart = true;

            var angle = Random.Range(0, 360);

            _rotate = Quaternion.Euler(0, angle, 0);
            _moveForward = _rotate * Vector3.forward;
        }

        if (Time.time > _startTime + Duration)
        {
            _isStart = false;
            return NodeState.Done;
        }

        _player.transform.rotation = Quaternion.RotateTowards(_player.transform.rotation, _rotate, RotateVelocity * Time.deltaTime);
        _player.GetComponent<Rigidbody>().velocity = _moveForward * Velocity +
            new Vector3(0, _player.GetComponent<Rigidbody>().velocity.y, 0);

        return NodeState.Running;
    }

    public override void Transition()
    {
        _isStart = false;
    }
}

public abstract class EnemyNode : BehaviourNode
{
    public PlayerCmpt _player;

    public EnemyNode(PlayerCmpt player)
    {
        _player = player;
    }

}
