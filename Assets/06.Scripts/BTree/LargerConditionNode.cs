﻿public class LargerConditionNode : BehaviourNode
{
    private int _value;
    private int _compareValue;

    public LargerConditionNode(int value, int compareValue)
    {
        _value = value;
        _compareValue = compareValue;
    }

    public override NodeState Tick()
    {
        if (_value > _compareValue)
            return NodeState.Done;
        return NodeState.Failure;
    }
}