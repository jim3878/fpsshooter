using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogNode : BehaviourNode
{
    private string _log;

    public LogNode(string log)
    {
        _log = log;
    }

    public override NodeState Tick()
    {
        Debug.Log(_log);
        return NodeState.Done;
    }
}
