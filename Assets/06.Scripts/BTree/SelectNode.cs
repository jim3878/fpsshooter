﻿public class SelectNode : BehaviourNode
{
    private int _currNodeIdx;

    public override NodeState Tick()
    {
        for (int i = 0; i < childNodes.Count; i++)
        {
            var currNode = childNodes[i];
            var result = currNode.Tick();
            if (result != NodeState.Failure)
            {
                if (_currNodeIdx >= 0 && i != _currNodeIdx)
                {
                    childNodes[_currNodeIdx].Transition();
                }
                _currNodeIdx = i;
                return result;
            }
        }

        _currNodeIdx = -1;
        return NodeState.Failure;
    }

    public override void Transition()
    {
        if (_currNodeIdx >= 0)
            childNodes[_currNodeIdx].Transition();
    }
}

