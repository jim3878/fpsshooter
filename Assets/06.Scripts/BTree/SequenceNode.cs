﻿public class SequenceNode : BehaviourNode
{
    private int _currIdx = -1;

    public override NodeState Tick()
    {
        if (_currIdx >= childNodes.Count)
        {
            _currIdx = -1;
            return NodeState.Done;
        }

        if (_currIdx < 0)
            _currIdx = 0;

        var currNode = childNodes[_currIdx];
        var result = currNode.Tick();
        if (result == NodeState.Done)
        {
            _currIdx++;
            return NodeState.Running;
        }
        if (result == NodeState.Failure)
        {
            return NodeState.Failure;
        }
        return NodeState.Running;

    }

    public override void Transition()
    {
        if (_currIdx < 0) return;
        childNodes[_currIdx].Transition();
    }
}
