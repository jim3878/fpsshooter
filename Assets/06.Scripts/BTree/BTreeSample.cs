using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BTreeSample : MonoBehaviour
{
    private bool _isDone;
    private BehaviourNode _root;

    void Start()
    {
        var randomValue = 40;
        _root = new SelectNode();

        var largerA = new SequenceNode()
            .AddChild(new LargerConditionNode(randomValue, 50))
            .AddChild(new LogNode(randomValue + ">" + 50));

        var largerB = new SequenceNode()
            .AddChild(new LargerConditionNode(randomValue, 0))
            .AddChild(new LogNode(randomValue + "<" + 50));

        _root.AddChild(largerA);
        _root.AddChild(largerB);
    }

    // Update is called once per frame
    void Update()
    {
        if (_isDone) return;
        var result = _root.Tick();
        if (result == NodeState.Done)
            _isDone = true;
    }
}
