﻿using UnityEngine;

public class IdleNode : EnemyNode
{
    private const float Duration = 2f;
    private bool _isStart;
    private float _startTime;

    public IdleNode(PlayerCmpt player) : base(player)
    {
    }

    public override NodeState Tick()
    {
        if (!_isStart)
        {
            _startTime = Time.time;
            _isStart = true;
        }

        if (Time.time > _startTime + Duration)
        {
            _isStart = false;
            return NodeState.Done;
        }

        return NodeState.Running;
    }
}
